# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import re
EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

class UserManager(models.Manager):
    def validation(self, postData):
        errors = {}
        if len(postData['first_name']) < 2:
            errors['first_name'] = "First name must be longer than two charcaters!"
        if len(postData['last_name']) < 2:
            errors['last_name'] = "Last name must be longer than two characters!"
        if not EMAIL_REGEX.match(postData['email']):
            errors['email'] = "Invalid email!"
        if len(postData['password']) != len(postData['cpassword']):
            errors['password'] = "Passwords do not match!"
        if len(postData['password']) < 3:
            errors['password'] = "Password must be longer than three characters"
        return errors
        

class User(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    cpassword = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()
    def __repr__(self):
        return "<User object {} {} {}>".format(self.first_name, self.last_name, self.email)
