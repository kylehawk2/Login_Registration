# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, HttpResponse, redirect
import bcrypt
from django.contrib import messages
from models import User, UserManager

def index(request):
    return render(request, 'login_reg/index.html')

def register(request):
    if request.method == "POST":
        errors = User.objects.validation(request.POST)
        if len(errors):
            for tag, error in errors.iteritems():
                messages.error(request, error, extra_tags=tag)
            return redirect('/')
        else:
            print "Registration Complete!"
            hash1 = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt())
            User.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'], password=hash1)
            messages.error(request, "Registration Complete!")
            return redirect('/success')
            
def login(request):
    email = request.POST['email']
    password = request.POST['password']
    user = User.objects.filter(email=email)
    if len(user) == 0:
        messages.error(request, "Invalid Email")
    else:
        if( bcrypt.checkpw(password.encode(), user[0].password.encode()) ):
            print "Password matches"
            request.session['email'] = email
            request.session['id'] = user[0].id
            return redirect('/success')
        else:
            messages.error(request, "Invalid password!")
            return redirect('/')

def success(request):
    return render(request, 'login_reg/success.html', { "user" : User.objects.get(id=request.session['id']) })

def logout(request):
    request.session.flush()
    return redirect('/')